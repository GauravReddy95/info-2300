Step 1 - Create a BitBucket Account.
Step 2 - Set it to public access.
Step 3 - Add a License to the account.
Step 4 - Download the repositary and use the GIT bash to push the changes.


Why MIT License -
MIT License is a free software license originating at the Massachusetts Institute of Technology.
It was created in late 1980's. It puts a very limited restriction on reuse and has high
license compatibility.
It can also be re-licensed under other licenses which is adds to its advantages.
As of 2020, MIT is the most used free license.